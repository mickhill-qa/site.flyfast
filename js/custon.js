// jQuery for page scrolling feature - requires jQuery Easing plugin
$(function() {
    $('.page-scroll a').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });
});

// Floating label headings for the contact form
$(function() {
    $("body").on("input propertychange", ".floating-label-form-group", function(e) {
        $(this).toggleClass("floating-label-form-group-with-value", !! $(e.target).val());
    }).on("focus", ".floating-label-form-group", function() {
        $(this).addClass("floating-label-form-group-with-focus");
    }).on("blur", ".floating-label-form-group", function() {
        $(this).removeClass("floating-label-form-group-with-focus");
    });
});

// Highlight the top nav as scrolling occurs
$('body').scrollspy({
    target: '.navbar-fixed-top'
})

// Closes the Responsive Menu on Menu Item Click
$('.navbar-collapse ul li a').click(function() {
    $('.navbar-toggle:visible').click();
});

        
// Clique do botão menu em smartphone
$('button').on('click', function() {
    $(this).toggleClass('isActive');
});



// Configuração do botão que volta para o topo.
$(document).ready(function()
{
    $('body').append('<div id="btn-to-top" class="btn btn-info"><i class=" fa fa-angle-double-up"></i></div>');
    $(window).scroll(function ()
    {
        if ($(this).scrollTop() != 0)
        {
            $('#btn-to-top').fadeIn();
        }
        else
        {
            $('#btn-to-top').fadeOut();
        }
    });

    $('#btn-to-top').click(function()
    {
        $("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });
});